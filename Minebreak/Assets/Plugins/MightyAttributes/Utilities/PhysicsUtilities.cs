﻿using UnityEngine;

namespace MightyAttributes.Utilities
{
    public static class PhysicsUtilities
    {
        public static bool FullyContains(Collider2D first, Collider2D second) =>
            first.OverlapPoint(second.bounds.min) && first.OverlapPoint(second.bounds.max);

        public static void RotateAround2D(Rigidbody2D rigidbody2D, Vector3 origin, Vector3 axis, float angle) =>
            RotateAround2D(rigidbody2D, origin, Quaternion.AngleAxis(angle, axis));

        public static void RotateAround2D(Rigidbody2D rigidbody2D, Vector2 origin, Quaternion angleAxis)
        {
            rigidbody2D.MovePosition(rigidbody2D.GetPositionOriginAngleAxis(origin, angleAxis));
            rigidbody2D.MoveRotation(rigidbody2D.transform.rotation * angleAxis);
        }

        public static Vector3 GetPositionOriginAngleAxis(this Rigidbody2D rigidbody2D, Vector2 origin, Quaternion angleAxis) =>
            angleAxis * (rigidbody2D.position - origin) + (Vector3) origin;
    }
}