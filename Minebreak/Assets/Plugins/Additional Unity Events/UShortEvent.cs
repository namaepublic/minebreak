﻿using System;
using UnityEngine.Events;

[Serializable]
public class UShortEvent : UnityEvent<ushort>
{
}