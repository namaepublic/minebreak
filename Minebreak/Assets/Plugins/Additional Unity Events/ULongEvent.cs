﻿using System;
using UnityEngine.Events;

[Serializable]
public class ULongEvent : UnityEvent<ulong>
{
}