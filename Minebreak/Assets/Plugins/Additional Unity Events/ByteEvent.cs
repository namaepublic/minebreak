﻿using System;
using UnityEngine.Events;

[Serializable]
public class ByteEvent : UnityEvent<byte>
{
}
