﻿using System;
using UnityEngine;
using UnityEngine.Events;

[Serializable]
public class EdgeCollider2DEvent : UnityEvent<EdgeCollider2D>
{
}
