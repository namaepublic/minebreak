﻿using System;
using UnityEngine;
using UnityEngine.Events;

[Serializable]
public class TwoVector2Event : UnityEvent<Vector2, Vector2>
{
}
