﻿using System.Collections.Generic;
using MightyAttributes;
#if UNITY_EDITOR
using EncryptionTool.Editor;
using System.IO;
using System.Linq;
using JetBrains.Annotations;
using MightyAttributes.Editor;
using UnityEditor;
#endif
using UnityEngine;

namespace EncryptionTool
{
    public class EncryptionInitializer : MonoBehaviour
    {
#if UNITY_EDITOR
        private const string FileName = "EncryptionBackup.xml";
#endif
        [SerializeField] private int _passwordSize = 10, _saltSize = 16;
        [SerializeField] private bool _keyAndIvIdentical;
        [SerializeField] private EncryptionData _encryptionData;
        [SerializeField, Hide] private List<EncryptionData> _backupEncryptionData;
        [SerializeField, Hide] private int _restoreIndex;

#if UNITY_EDITOR
        private SerializableEncryptionDataBackup m_serializedBackup;
#endif

        public byte[] KeySalt => _encryptionData.KeySalt;
        public byte[] IvSalt => _encryptionData.IvSalt;
        public string Password => _encryptionData.Password;

#if UNITY_EDITOR

        #region Editor

        private bool m_unfoldBackup;

        private void OnValidate()
        {
            if (_saltSize < 8) _saltSize = 8;
            if (_passwordSize < 8) _passwordSize = 8;
            AddCurrentToBackup();
        }

        private void AddCurrentToBackup()
        {
            if (_encryptionData != null && !_encryptionData.IsEmpty() && !_backupEncryptionData.Any(x => x.Compare(_encryptionData)))
            {
                _backupEncryptionData.Add(new EncryptionData(_encryptionData));
                _restoreIndex = _backupEncryptionData.Count - 1;
            }

            RemoveDuplicatesBackup();
            SyncWithFile();
        }

        private void RemoveDuplicatesBackup()
        {
            if (_backupEncryptionData == null) return;
            
            var index = 0;
            while (true)
            {
                if (index >= _backupEncryptionData.Count) return;
                var duplicateCount = 0;
                foreach (var backup in _backupEncryptionData)
                    if (backup.Compare(_backupEncryptionData[index]))
                        duplicateCount++;
                if (duplicateCount > 1)
                    _backupEncryptionData.RemoveAt(index);
                else
                    index++;
            }
        }

        private void Generate()
        {
            if (!_encryptionData.IsEmpty() &&
                !EditorUtility.DisplayDialog("Generate Encryption Data", "Are you sure to overwrite current Encryption Data?",
                    "Overwrite", "Cancel"))
                return;
            AddCurrentToBackup();
            _encryptionData = new EncryptionData(EncryptionHelpers.GenerateString(_passwordSize),
                EncryptionHelpers.GenerateByteArray(_saltSize),
                _keyAndIvIdentical ? null : EncryptionHelpers.GenerateByteArray(_saltSize));
            AddCurrentToBackup();
        }

        private void RestorePrevious() => NavigateRestore(false);

        private void RestoreNext() => NavigateRestore(true);

        private void NavigateRestore(bool next)
        {
            if (_backupEncryptionData == null || _backupEncryptionData.Count == 0) return;
            if (_restoreIndex > _backupEncryptionData.Count - 1) _restoreIndex = _backupEncryptionData.Count - 1;

            if (_encryptionData.Compare(_backupEncryptionData[_restoreIndex]))
            {
                if (next)
                {
                    if (_restoreIndex < _backupEncryptionData.Count - 1)
                        _encryptionData = _backupEncryptionData[++_restoreIndex];
                    SyncWithFile();
                    return;
                }

                if (_restoreIndex > 0)
                    _encryptionData = _backupEncryptionData[--_restoreIndex];
                SyncWithFile();
                return;
            }

            _encryptionData = _backupEncryptionData[_restoreIndex];

            if (next && _restoreIndex < _backupEncryptionData.Count - 1)
                _restoreIndex++;
            else if (_restoreIndex > 0)
                _restoreIndex--;

            SyncWithFile();
        }

        private void SyncWithFile()
        {
            var script = MonoScript.FromMonoBehaviour(this);
            var dirInfo = new DirectoryInfo(AssetDatabase.GetAssetPath(script));
            var path = dirInfo.Parent?.Parent?.FullName + "\\" + FileName;
            if (File.Exists(path))
            {
                m_serializedBackup = File.ReadAllText(path).XmlDeserialize<SerializableEncryptionDataBackup>();
                MergeBackups(_backupEncryptionData, m_serializedBackup.BackupEncryptionData);
                if (!_encryptionData.IsEmpty())
                    m_serializedBackup.EncryptionData = new SerializableEncryptionData(_encryptionData);
                else if (!m_serializedBackup.EncryptionData.IsEmpty())
                    _encryptionData = new EncryptionData(m_serializedBackup.EncryptionData);
                File.WriteAllText(path, m_serializedBackup.XmlSerialize());
            }
            else
            {
                m_serializedBackup = new SerializableEncryptionDataBackup(_encryptionData, _backupEncryptionData);
                File.WriteAllText(path, m_serializedBackup.XmlSerialize());
            }
        }

        private void MergeBackups([NotNull] List<EncryptionData> localBackup, [NotNull] List<SerializableEncryptionData> serializedBackup)
        {
            var localDifferences = new List<EncryptionData>();
            var serializedDifferences = new List<SerializableEncryptionData>();
            foreach (var backup in localBackup)
                if (!serializedBackup.Any(x => x.Compare(backup)))
                    localDifferences.Add(backup);
            foreach (var backup in serializedBackup)
                if (!localBackup.Any(x => backup.Compare(x)))
                    serializedDifferences.Add(backup);
            foreach (var difference in localDifferences)
                serializedBackup.Add(new SerializableEncryptionData(difference));
            foreach (var difference in serializedDifferences)
                localBackup.Add(new EncryptionData(difference));
        }

        [CustomEditor(typeof(EncryptionInitializer))]
        public class EncryptionInitializerInspector : BaseMightyEditor
        {
            private int _selectIndex;
            private EncryptionInitializer m_encryptionInitializer;

            private void OnEnable() => m_encryptionInitializer = (EncryptionInitializer) target;

            public override void OnInspectorGUI()
            {
                base.OnInspectorGUI();
                EditorGUILayout.Space();

                if (GUILayout.Button("Generate Encryption Data"))
                    m_encryptionInitializer.Generate();

                EditorGUILayout.Space();
                EditorGUILayout.Space();
                EditorGUILayout.Space();
                GUILayout.BeginHorizontal();

                GUILayout.Label("Restore Backup:");
                if (GUILayout.Button("Previous", GUILayout.Width(100)))
                    m_encryptionInitializer.RestorePrevious();

                if (GUILayout.Button("Next", GUILayout.Width(100)))
                    m_encryptionInitializer.RestoreNext();

                GUILayout.Label($"Index: {m_encryptionInitializer._restoreIndex}");

                GUILayout.EndHorizontal();

                if (m_encryptionInitializer.m_unfoldBackup =
                    EditorGUILayout.Foldout(m_encryptionInitializer.m_unfoldBackup, "Backup Encryption Data", true) &&
                    m_encryptionInitializer._backupEncryptionData != null)
                    foreach (var backup in m_encryptionInitializer._backupEncryptionData)
                        GUILayout.Label(backup.Password);

                var script = MonoScript.FromMonoBehaviour(m_encryptionInitializer);
                var dirInfo = new DirectoryInfo(AssetDatabase.GetAssetPath(script));
                var path = dirInfo.Parent?.Parent?.FullName + "\\" + FileName;
                GUI.enabled = false;
                EditorGUILayout.LabelField(path);
                GUI.enabled = true;
            }
        }

        #endregion /Editor

#endif
    }
}