﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Numerics;
using EncryptionTool;
using Newtonsoft.Json;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

public enum SavedDataType : byte
{
    PlayerData,
    SettingsData
}

public class BigIntegerConverter : JsonConverter
{
    public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer) => writer.WriteValue(value.ToString());

    public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer) =>
        BigInteger.TryParse(reader.Value.ToString(), out var bigInteger) ? bigInteger : existingValue;

    public override bool CanConvert(Type objectType) => objectType == typeof(BigInteger);
}

#region Data

[Serializable]
public struct PlayerData
{
    [JsonConverter(typeof(BigIntegerConverter))]
    public BigInteger moneyAmount;

    public ulong stealCount;

    public Dictionary<ushort, ushort> speedLevelByRegion;
    public Dictionary<ushort, ushort> unitLevelByRegion;
    public Dictionary<ushort, ushort> strengthLevelByRegion;

    public byte regionIndex;
    public byte levelIndex;
}

[Serializable]
public struct SettingsData
{
    public bool mute;
}

#endregion /Data

public class SavedDataServices : MonoBehaviour
{
    public const string GAME_NAME = "DemolitionBall";

    public static readonly string PlayerDataFileName = $"{GAME_NAME}PlayerData";
    public static readonly string SettingsDataFileName = $"{GAME_NAME}SettingsData";

    private static bool m_init;

    private static string m_playerDataPath, m_settingsDataPath;

    private static bool m_savePlayer, m_saveSettings;

    private static PlayerData m_playerData;
    private static SettingsData m_settingsData;

    public static PlayerData PlayerData
    {
        get
        {
            if (!m_init) InitManagedPlayerData(true);
            return m_playerData;
        }
    }
    
    #region Static Access

    #region PlayerData
    
        public static BigInteger MoneyAmount
    {
        get => PlayerData.moneyAmount;
        set
        {
            if (PlayerData.moneyAmount == value) return;

            m_playerData.moneyAmount = value;
            
            m_savePlayer = true;
            SaveToLocal(SavedDataType.PlayerData);
        }
    }

    public static ulong ImpactCount
    {
        get => PlayerData.stealCount;
        set
        {
            if (PlayerData.stealCount == value) return;

            m_playerData.stealCount = value;
            
            m_savePlayer = true;
            SaveToLocal(SavedDataType.PlayerData);
        }
    }

    public static bool TryGetUpgradeLevel(UpgradeType type, ushort regionIndex, out ushort level)
    {
        level = 0;
        switch (type)
        {
            case UpgradeType.Bounciness:
                if (!PlayerData.speedLevelByRegion.ContainsKey(regionIndex)) return false;
                level = m_playerData.speedLevelByRegion[regionIndex];
                return true;
            case UpgradeType.Unit:
                if (!PlayerData.unitLevelByRegion.ContainsKey(regionIndex)) return false;
                level = m_playerData.unitLevelByRegion[regionIndex];
                return true;
            case UpgradeType.Damage:
                if (!PlayerData.strengthLevelByRegion.ContainsKey(regionIndex)) return false;
                level = m_playerData.strengthLevelByRegion[regionIndex];
                return true;
        }

        return false;
    }
    
    public static void IncreaseUpgradeLevel(UpgradeType type, ushort regionIndex)
    {
        switch (type)
        {
            case UpgradeType.Bounciness:
            {
                if (PlayerData.speedLevelByRegion.ContainsKey(regionIndex))
                    m_playerData.speedLevelByRegion[regionIndex]++;
                else
                    m_playerData.speedLevelByRegion[regionIndex] = 0;
                
                m_savePlayer = true;
                SaveToLocal(SavedDataType.PlayerData);

                break;
            }
            case UpgradeType.Unit:
            {
                if (PlayerData.unitLevelByRegion.ContainsKey(regionIndex))
                    m_playerData.unitLevelByRegion[regionIndex]++;
                else
                    m_playerData.unitLevelByRegion[regionIndex] = 0;
                
                m_savePlayer = true;
                SaveToLocal(SavedDataType.PlayerData);

                break;
            }
            case UpgradeType.Damage:
            {
                if (PlayerData.strengthLevelByRegion.ContainsKey(regionIndex))
                    m_playerData.strengthLevelByRegion[regionIndex]++;
                else
                    m_playerData.strengthLevelByRegion[regionIndex] = 0;
                
                m_savePlayer = true;
                SaveToLocal(SavedDataType.PlayerData);

                break;
            }
        }
    }
    
    public static void ResetUpgradeLevel(UpgradeType type, ushort regionIndex)
    {
        switch (type)
        {
            case UpgradeType.Bounciness:
            {
                if (PlayerData.speedLevelByRegion.TryGetValue(regionIndex, out var currentLevel) && currentLevel == 0) break;

                m_playerData.speedLevelByRegion[regionIndex] = 0;
                
                m_savePlayer = true;
                SaveToLocal(SavedDataType.PlayerData);

                break;
            }
            case UpgradeType.Unit:
            {
                if (PlayerData.unitLevelByRegion.TryGetValue(regionIndex, out var currentLevel) && currentLevel == 0) break;

                m_playerData.unitLevelByRegion[regionIndex] = 0;
                
                m_savePlayer = true;
                SaveToLocal(SavedDataType.PlayerData);

                break;
            }
            case UpgradeType.Damage:
            {
                if (PlayerData.strengthLevelByRegion.TryGetValue(regionIndex, out var currentLevel) && currentLevel == 0) break;

                m_playerData.strengthLevelByRegion[regionIndex] = 0;
                
                m_savePlayer = true;
                SaveToLocal(SavedDataType.PlayerData);

                break;
            }
        }
    }

    public static byte RegionIndex
    {
        get => PlayerData.regionIndex;
        set
        {
            if (PlayerData.regionIndex == value) return;

            m_playerData.regionIndex = value;

            m_savePlayer = true;
            SaveToLocal(SavedDataType.PlayerData);
        }
    }

    public static byte LevelIndex
    {
        get => PlayerData.levelIndex;
        set
        {
            if (PlayerData.levelIndex == value) return;

            m_playerData.levelIndex = value;

            m_savePlayer = true;
            SaveToLocal(SavedDataType.PlayerData);
        }
    }

    #endregion /PlayerData

    #region SettingsData

    public static bool Mute
    {
        get => m_settingsData.mute;
        set
        {
            if (m_settingsData.mute == value) return;

            m_saveSettings = true;
            m_settingsData.mute = value;
        }
    }

    #endregion /SettingsData

    #endregion /Static Access

    #region Utilities

    #region IO

    private static string GetPath(SavedDataType dataType)
    {
        switch (dataType)
        {
            case SavedDataType.PlayerData:
                if (string.IsNullOrEmpty(m_playerDataPath))
                    m_playerDataPath = $"{Path.Combine(Application.persistentDataPath, PlayerDataFileName)}.save";
                return m_playerDataPath;
            case SavedDataType.SettingsData:
                if (string.IsNullOrEmpty(m_settingsDataPath))
                    m_settingsDataPath = $"{Path.Combine(Application.persistentDataPath, SettingsDataFileName)}.save";
                return m_settingsDataPath;
            default:
                return null;
        }
    }

    private static void CreateFile(SavedDataType dataType)
    {
        switch (dataType)
        {
            case SavedDataType.PlayerData:
                File.WriteAllText(GetPath(SavedDataType.PlayerData), Encryption.Encrypt(DataToJson(SavedDataType.PlayerData)));
                break;
            case SavedDataType.SettingsData:
                File.WriteAllText(GetPath(SavedDataType.SettingsData), DataToJson(SavedDataType.SettingsData));
                break;
        }
    }

    #endregion /IO

    #region Json

    private static string DataToJson(SavedDataType dataType)
    {
        switch (dataType)
        {
            case SavedDataType.PlayerData:
                return JsonConvert.SerializeObject(m_playerData);
            case SavedDataType.SettingsData:
                return JsonConvert.SerializeObject(m_settingsData);
            default:
                return null;
        }
    }    
    
    private static void JsonToData(string json, SavedDataType dataType)
    {
        switch (dataType)
        {
            case SavedDataType.PlayerData:
                m_playerData = JsonConvert.DeserializeObject<PlayerData>(json);
                InitManagedPlayerData(true);
                break;
            case SavedDataType.SettingsData:
                m_settingsData = JsonConvert.DeserializeObject<SettingsData>(json);
                break;
        }
    }

    #endregion /Json

    #endregion /Utilities

    #region Init

    public static void Init(EncryptionInitializer encryptionInitializer) => Encryption.Init(encryptionInitializer);

    private static void InitManagedPlayerData(bool checkIfNull)
    {
        m_init = true;
        if (checkIfNull)
        {
            // LoadModel Managed Data if Null
            if (m_playerData.speedLevelByRegion == null) m_playerData.speedLevelByRegion = new Dictionary<ushort, ushort>();
            if (m_playerData.unitLevelByRegion == null) m_playerData.unitLevelByRegion = new Dictionary<ushort, ushort>();
            if (m_playerData.strengthLevelByRegion == null) m_playerData.strengthLevelByRegion = new Dictionary<ushort, ushort>();
            return;
        }

        // Always LoadModel Managed Data
        m_playerData.speedLevelByRegion = new Dictionary<ushort, ushort>();
        m_playerData.unitLevelByRegion = new Dictionary<ushort, ushort>();
        m_playerData.strengthLevelByRegion = new Dictionary<ushort, ushort>();
    }

    #endregion /Init

    #region LoadModel & Save

    private static bool UpdatePlayerData(PlayerData playerData)
    {
        var localKept = false;

        // Compare and update each fields and choose whether to keel local or not

        return localKept;
    }

    public static void LoadEverythingFromLocal()
    {
        LoadFromLocal(SavedDataType.PlayerData);
        LoadFromLocal(SavedDataType.SettingsData);
    }

    public static void LoadFromLocal(SavedDataType dataType)
    {
        var path = GetPath(dataType);

        if (!File.Exists(path))
        {
            if (dataType == SavedDataType.PlayerData) InitManagedPlayerData(false);

            CreateFile(dataType);
            return;
        }

        try
        {
            switch (dataType)
            {
                case SavedDataType.PlayerData:
                    JsonToData(Encryption.Decrypt(File.ReadAllText(path)), dataType);
                    break;
                case SavedDataType.SettingsData:
                    JsonToData(File.ReadAllText(path), dataType);
                    break;
            }
        }
        catch
        {
            if (dataType == SavedDataType.PlayerData)
                InitManagedPlayerData(true);
            SaveToLocal(dataType);
        }

#if UNITY_EDITOR
        Debug.Log(path);
        Debug.Log(DataToJson(dataType));
#endif
    }

    public static void SavePlayerToLocal() => SaveToLocal(SavedDataType.PlayerData);

    public static void SavesSettingsToLocal() => SaveToLocal(SavedDataType.SettingsData);

    public static void SaveToLocal(SavedDataType dataType)
    {
        switch (dataType)
        {
            case SavedDataType.PlayerData:
                if (!m_savePlayer) return;
                m_savePlayer = false;
                break;
            case SavedDataType.SettingsData:
                if (!m_saveSettings) return;
                m_saveSettings = false;
                break;
        }

        CreateFile(dataType);
    }

    #endregion /Load & Save

    #region Reset

    public static void ResetData(SavedDataType dataType)
    {
        switch (dataType)
        {
            case SavedDataType.PlayerData:
                ResetPlayerData();
                break;
            case SavedDataType.SettingsData:
                ResetSettingsData();
                break;
        }
    }

#if UNITY_EDITOR
    [MenuItem("Tools/Saved Data Services/Reset Player Data", false, 11)]
#endif
    public static void ResetPlayerData()
    {
        m_playerData = new PlayerData();
        InitManagedPlayerData(false);
        var path = GetPath(SavedDataType.PlayerData);
        if (!File.Exists(path)) return;
        File.Delete(path);
    }

#if UNITY_EDITOR
    [MenuItem("Tools/Saved Data Services/Reset Settings Data", false, 12)]
#endif
    public static void ResetSettingsData()
    {
        m_settingsData = new SettingsData();
        var path = GetPath(SavedDataType.SettingsData);
        if (!File.Exists(path)) return;
        File.Delete(path);
    }

    #endregion /Reset
}