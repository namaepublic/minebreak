﻿using TMPro;
using UnityEngine;

public class LevelBarBehaviour : BaseProgressBarBehaviour
{
    [SerializeField] private TextMeshProUGUI _levelText;

    public override void Init()
    {
        RefreshLevelText(InstanceManager.PlayerLevelManager.CurrentLevel);
        SetProgression(InstanceManager.PlayerLevelManager.GetLevelProgression(InstanceManager.PlayerLevelManager.CurrentXP));
    }

    public void RefreshLevelText(ushort level) => _levelText.text = (level + 1).ToString();

    protected override void Upgrade()
    {
    }
}