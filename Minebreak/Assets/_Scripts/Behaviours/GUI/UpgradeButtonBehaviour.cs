﻿using MightyAttributes;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class UpgradeButtonBehaviour : BaseBehaviour
{
    private const string MAX = "MAX";
    [SerializeField] private UpgradeType _type;

    [Title("Components")] [SerializeField] private Button _button;
    [SerializeField] private TextMeshProUGUI _nameText;
    [SerializeField] private Image _icon;
    [SerializeField] private TextMeshProUGUI _levelText;
    [SerializeField] private TextMeshProUGUI _costText;

    private UpgradesManager m_upgradesManager;

    public UpgradeType Type => _type;

    [Button]
    public override void Init()
    {
        m_upgradesManager = InstanceManager.UpgradesManager;
        
        if (!m_upgradesManager.TryGetUpgrade(_type, out var upgrade)) return;

        _nameText.text = upgrade.Name;
        _nameText.color = upgrade.Color;
        
        _icon.sprite = upgrade.Icon;
        _icon.color = upgrade.Color;
    }

    public void SetEnable(bool enable) => _button.interactable = enable;

    public void UpdateLevel()
    {
        if (!m_upgradesManager.TryGetUpgrade(_type, out var upgrade)) return;
        var regionIndex = InstanceManager.RegionManager.RegionIndex;
        
        var level = m_upgradesManager.GetLevel(_type);

        _levelText.text = (level + 1).ToString();
        
        if (!upgrade.CanUpgrade(regionIndex, level))
        {
            _costText.text = MAX;
            SetEnable(false);
        }
        else
            _costText.text = upgrade.GetCost(regionIndex, level).DisplayAsPowerOf10();
    }

    public void OnButtonClicked()
    {
        if (m_upgradesManager.TryToBuy(_type))
            UpdateLevel();
    }
}