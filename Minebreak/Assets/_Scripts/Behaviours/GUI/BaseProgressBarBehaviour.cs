﻿using UnityEngine;
using UnityEngine.UI;

public abstract class BaseProgressBarBehaviour : MonoBehaviour
{
    [SerializeField] private Image _fillArea;

    public abstract void Init();

    public void SetProgression(float progression)
    {
        if (progression >= 1) Upgrade();
        else _fillArea.fillAmount = progression;
    }

    protected abstract void Upgrade();
}