﻿using UnityEngine;
using UnityEngine.UI;

public class RegionBarBehaviour : BaseProgressBarBehaviour
{
    [SerializeField] private GameObject _progressPanel;
    [SerializeField] private Button _nextRegionButton;
    [SerializeField] private GameObject _nextRegionLabel;
    [SerializeField] private GameObject _comingSoonLabel;

    public override void Init()
    {
        _progressPanel.SetActive(true);
        _nextRegionButton.gameObject.SetActive(false);
    }

    protected override void Upgrade()
    {
        _progressPanel.SetActive(false);
        _nextRegionButton.gameObject.SetActive(true);
        
        var nextRegionAvailable = InstanceManager.GameManager.NextRegionAvailable();
        
        _nextRegionButton.interactable = nextRegionAvailable;
        _nextRegionLabel.SetActive(nextRegionAvailable);
        _comingSoonLabel.SetActive(!nextRegionAvailable);
    }

    public void OnClickNextRegion() => InstanceManager.GUIManager.BeginTransitionToNextRegion();
}
