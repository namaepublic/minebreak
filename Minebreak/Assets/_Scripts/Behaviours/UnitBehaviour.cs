﻿using MightyAttributes;
using MightyAttributes.Utilities;
using UnityEngine;

public class UnitBehaviour : BaseBehaviour
{
    [SerializeField, GetComponent, ReadOnly]
    private Rigidbody2D _rigidbody;

    [SerializeField, GetComponentInChildren, ReadOnly]
    private TrailRenderer _trail;

    [SerializeField, GetComponentInChildren, ReadOnly]
    private PoolBehaviour _particlesPool;

    public float bounceTreshold;

    private float m_impactDamage;
    private float m_bounceForce;

    private GameObject m_gameObject;

    public override void Init()
    {
        m_gameObject = gameObject;
        m_gameObject.SetActive(false);

        _trail.enabled = false;
        _rigidbody.isKinematic = true;

        _particlesPool.Init();
    }

    public void Spawn(Vector3 position)
    {
        transform.position = position;

        _trail.enabled = true;
        _trail.Clear();

        m_gameObject.SetActive(true);
    }

    public void Release() => _rigidbody.isKinematic = false;

    public void SetImpactDamage(float damage) => m_impactDamage = damage;
    public void SetBounceForce(float force) => m_bounceForce = force;

    private void OnCollisionEnter2D(Collision2D collision)
    {
        var collidedObject = collision.gameObject;
        var layer = collidedObject.layer;
        if (layer != StaticLayers.WALL_LAYER && layer != StaticLayers.DESTRUCTIBLE_LAYER) return;

        var relativeVelocity = collision.relativeVelocity;

        float relativeSqrMagnitude;

        if ((relativeSqrMagnitude = VectorUtilities.SqrMagnitude(relativeVelocity)) < bounceTreshold &&
            layer != StaticLayers.DESTRUCTIBLE_LAYER) return;

        var normalizedRelative = VectorUtilities.Normalize(relativeVelocity, VectorUtilities.Magnitude(relativeSqrMagnitude));

        var contactNormal = collision.GetContact(0).normal;
        var bounceDirection = -Vector2.Reflect(normalizedRelative, contactNormal);

        _rigidbody.AddForce(bounceDirection * m_bounceForce, ForceMode2D.Impulse);

        _particlesPool.ActivateNext();

        if (layer == StaticLayers.DESTRUCTIBLE_LAYER)
            InstanceManager.DestructibleManager.OnImpact(collidedObject.GetInstanceID(), m_impactDamage);
    }
}