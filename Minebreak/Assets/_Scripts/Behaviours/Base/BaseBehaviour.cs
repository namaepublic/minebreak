﻿using UnityEngine;

public abstract class BaseBehaviour : MonoBehaviour
{
    public abstract void Init();
}
