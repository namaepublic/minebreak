﻿using Destructible2D;
using MightyAttributes;
using MightyAttributes.Utilities;
using UnityEngine;

public class ChunkBehaviour : BaseBehaviour
{
    [SerializeField, GetComponent, ReadOnly]
    private Rigidbody2D _rigidbody;

    [SerializeField, GetComponent, ReadOnly]
    private D2dDestructibleSprite _destructible;

    [SerializeField, GetComponent, ReadOnly]
    private D2dDamage _damage;

    public Destructible Destructible { get; set; }

    public override void Init()
    {
        _rigidbody.constraints = RigidbodyConstraints2D.FreezeAll;
        _destructible.OnSplitEnd += InstanceManager.DestructibleManager.OnSplitEnd;
    }

    public void EjectFromInitialChunk(float force) => Eject(VectorUtilities.Normalize(Destructible.InitialPosition - _rigidbody.position) * force);

    public void Eject(float force) => Eject(Random.insideUnitCircle * force);

    private void Eject(Vector2 velocity)
    {
        _rigidbody.constraints = RigidbodyConstraints2D.None;
        _rigidbody.AddForce(velocity, ForceMode2D.Impulse);
    }

    public void IncreaseDamage(float amount) => _damage.Damage += amount;
}