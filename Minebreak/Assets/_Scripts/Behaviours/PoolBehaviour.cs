﻿using MightyAttributes;
using UnityEngine;

public class PoolBehaviour : BaseBehaviour
{
    [SerializeField] private GameObject[] _objects;

    private int m_index;
    
    public override void Init()
    {
        m_index = 0;
        foreach (var obj in _objects) obj.SetActive(false);
    }

    public void ActivateNext()
    {
        ActivateObject(m_index++);
        if (m_index == _objects.Length) m_index = 0;
    }

    public void ActivateObject(int index, bool deactivateOthers = false)
    {
        if (!deactivateOthers)
        {
            _objects[index].SetActive(true);
            return;
        }

        for (var i = 0; i < _objects.Length; i++) 
            _objects[i].SetActive(i == index);
    }

#if UNITY_EDITOR
    [Button]
    private void PopulateWithChildren()
    {
        var tfm = transform;
        var count = tfm.childCount;
        _objects = new GameObject[count];

        for (var i = 0; i < count; i++)
            _objects[i] = transform.GetChild(i).gameObject;
    }
#endif
}