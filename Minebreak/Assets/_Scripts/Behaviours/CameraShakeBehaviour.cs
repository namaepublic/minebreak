﻿using Cinemachine;
using MightyAttributes;
using UnityEngine;

public class CameraShakeBehaviour : BaseBehaviour
{
    [SerializeField, GetComponentInChildren, ReadOnly]
    private CinemachineBasicMultiChannelPerlin _cameraNoise;

    [SerializeField] private AnimationCurve _shakeCurve;
    public float duration;

    private bool m_shaking;
    private float m_timer;

    public override void Init()
    {
        m_shaking = false;
        m_timer = 0;
        
        _shakeCurve.postWrapMode = WrapMode.ClampForever;
    }

    public void Shake()
    {
        m_shaking = true;

        m_timer = 0;
        _cameraNoise.m_AmplitudeGain = 0;
    }

    public void StopShake()
    {
        m_shaking = false;

        m_timer = 0;
        _cameraNoise.m_AmplitudeGain = 0;
    }

    public void UpdateBehaviour()
    {
        if (!m_shaking) return;

        _cameraNoise.m_AmplitudeGain = _shakeCurve.Evaluate(m_timer / duration);

        m_timer += Time.deltaTime;

        if (m_timer >= duration) StopShake();
    }
}