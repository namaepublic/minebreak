﻿using UnityEngine;

public class TransitionBehaviour : MonoBehaviour
{
    public void LevelInTransition() => InstanceManager.RegionManager.LoadNextLevel();

    public void LevelOutTransition() => InstanceManager.LevelManager.BeginLevel();

    public void RegionInTransition() => InstanceManager.GameManager.LoadNextRegion();

    public void RegionOutTransition() => InstanceManager.RegionManager.BeginRegion();
}