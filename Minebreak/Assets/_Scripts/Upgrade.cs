﻿using System;
using System.Linq;
using System.Numerics;
using MightyAttributes;
using UnityEngine;

public enum UpgradeType : byte
{
    Bounciness,
    Damage,
    Unit,
}

[Line(ArrayDecoratorPosition.BeforeElements)]
[ItemNames("GetRegionNames", true, ArrayOption.HideSizeField | ArrayOption.DontFold)]
[Nest(NestOption.DontFold | NestOption.BoldLabel | NestOption.DontIndent)]
public class UpgradesAttribute : BaseWrapperAttribute
{
#if UNITY_EDITOR
    private string[] GetRegionNames() =>
        ReferencesUtilities.FindAssetsOfType<RegionModel>().OrderBy(r => r.Index).Select(a => a.name).ToArray();
#endif
}

[Serializable]
public class UpgradeCurve
{
    [SerializeField, MinValue(0)] private ushort _maxLevel;

    [SerializeField, MinValue(0)] private ulong _baseCost;
    [SerializeField] private AnimationCurve _costCurve;

    [SerializeField] private float _defaultValue;
    [SerializeField] private AnimationCurve _valueCurve;

    public ushort MaxLevel => _maxLevel;

    public BigInteger GetCost(ushort level) => (BigInteger) (_baseCost * _costCurve.Evaluate((float) level / _maxLevel));

    public float GetValue(ushort level) =>
        _defaultValue + _valueCurve.Evaluate(level == 0 ? 0 : level == _maxLevel ? 1 : (float) level / _maxLevel);

    public float GetMaxValue() => _defaultValue + _valueCurve.Evaluate(1);
}

[CreateAssetMenu(fileName = "Upgrade", menuName = "Upgrade")]
public class Upgrade : ScriptableObject
{
    [Title("GFX")] [SerializeField] private string _name;
    [SerializeField] private Sprite _icon;
    [SerializeField] private Color _color;

    [Title("Upgrade")] [SerializeField] private UpgradeType _type;

    [SerializeField, Upgrades] private UpgradeCurve[] _upgradeCurves;

    public string Name => _name;
    public Sprite Icon => _icon;
    public Color Color => _color;

    public UpgradeType Type => _type;

    public bool CanUpgrade(byte regionIndex, ushort level) => level < _upgradeCurves[regionIndex].MaxLevel;

    public ushort GetMaxLevel(byte regionIndex) => _upgradeCurves[regionIndex].MaxLevel;

    public BigInteger GetCost(byte regionIndex, ushort level) => _upgradeCurves[regionIndex].GetCost(level);

    public float GetValue(byte regionIndex, ushort level) => _upgradeCurves[regionIndex].GetValue(level);

    public float GetMaxValue(byte regionIndex) => _upgradeCurves[regionIndex].GetMaxValue();
}