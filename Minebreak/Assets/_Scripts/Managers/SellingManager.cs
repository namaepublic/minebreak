﻿using System.Numerics;
using UnityEngine;

public class SellingManager : BaseManager
{
    [SerializeField] private FastProgressCurve _damagePriceCurve;

    public override void Init()
    {
    }

    private BigInteger GetDamagePrice(float damage) =>
        new BigInteger(_damagePriceCurve.Evaluate(InstanceManager.PlayerLevelManager.CurrentLevel) * damage);

    public void Sell(float damage)
    {
        var price = GetDamagePrice(damage);
        InstanceManager.MoneyManager.AddAmount(price);
    }
}