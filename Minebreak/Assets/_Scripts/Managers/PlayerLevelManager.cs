﻿using UnityEngine;

public class PlayerLevelManager : BaseManager
{
    [SerializeField] private SlowProgressCurve _playerLevelCurve;
    [SerializeField] private ULongEvent _onImpactCountChange;
    [SerializeField] private UShortEvent _onLevelUp;

    private ulong m_previousNeededXP, m_nextNeededXP, m_currentNeededXP;

    public ulong CurrentXP { get; private set; }
    public ushort CurrentLevel { get; private set; }

    public override void Init()
    {
        CurrentXP = SavedDataServices.ImpactCount;
        UpdateLevel();
    }

    public void IncreaseImpactCount()
    {
        SetXP(++SavedDataServices.ImpactCount);
        
        _onImpactCountChange.Invoke(CurrentXP);
    }

    public float GetLevelProgression(ulong currentXP) => (float) (currentXP - m_previousNeededXP) / m_currentNeededXP;

    private void SetXP(ulong newXP)
    {
        CurrentXP = newXP;

        if (CurrentXP < m_nextNeededXP) return;
        
        UpdateLevel();
        _onLevelUp.Invoke(CurrentLevel);
    }

    private void UpdateLevel()
    {
        CurrentLevel = GetPlayerLevel(CurrentXP);
            
        m_previousNeededXP = (ulong) GetNeededXP(CurrentLevel);
        m_nextNeededXP = (ulong) GetNeededXP((ushort) (CurrentLevel + 1));
        m_currentNeededXP = m_nextNeededXP - m_previousNeededXP;
    }

    private ushort GetPlayerLevel(ulong currentXP) => (ushort) _playerLevelCurve.Evaluate(currentXP);

    private double GetNeededXP(ushort level) => level > 0 ? _playerLevelCurve.Revert(level) : 0;
}