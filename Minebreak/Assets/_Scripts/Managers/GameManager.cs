﻿using EncryptionTool;
using MightyAttributes;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : BaseBrainManager
{
    [SerializeField, FindObject, ReadOnly] private EncryptionInitializer _encryptionInitializer;

    [SerializeField, FindAssets, Reorderable(false, options: ArrayOption.DontFold | ArrayOption.DisableSizeField)]
    private RegionModel[] _regions;

    private bool m_playing;

    private void Awake() => Init();

    private void Start() => LoadRegion(SavedDataServices.RegionIndex);

    public override void Init()
    {
        SavedDataServices.Init(_encryptionInitializer);
        SavedDataServices.LoadEverythingFromLocal();

        Pause();
        base.Init();
    }

    private void Update()
    {
        if (!m_playing) return;
        UpdateManager();
        InstanceManager.RegionManager.UpdateManager();
        InstanceManager.LevelManager.UpdateManager();
    }

    private void FixedUpdate()
    {
        if (!m_playing) return;
        FixedUpdateManager();
        InstanceManager.RegionManager.FixedUpdateManager();
        InstanceManager.LevelManager.FixedUpdateManager();
    }

    public void Play() => m_playing = true;

    public void Pause() => m_playing = false;

    public bool NextRegionAvailable() => SavedDataServices.RegionIndex + 1 < _regions.Length;

    public void LoadNextRegion()
    {
        var index = SavedDataServices.RegionIndex;
        if (index + 1 >= _regions.Length) return;

        InstanceManager.RegionManager.UnloadLevel(SavedDataServices.LevelIndex);
        SavedDataServices.LevelIndex = 0;

        if (IsRegionLoaded(index)) UnloadRegion(index);
        LoadRegion(++index);
    }

    public void LoadRegion(byte index, bool saveProgression = true)
    {
        if (index >= _regions.Length) return;

        if (IsRegionLoaded(index)) UnloadRegion(index);
        if (saveProgression) SavedDataServices.RegionIndex = index;

        SceneManager.LoadScene(_regions[index].SceneIndex, LoadSceneMode.Additive);
    }

    public void UnloadRegion(int index) => SceneManager.UnloadSceneAsync(_regions[index].SceneIndex);

    public bool IsRegionLoaded(byte index) => _regions[index].IsLoaded();

#if UNITY_EDITOR
    [OnInspectorGUI]
    private void PopulateRegions()
    {
        if (_regions == null) return;

        for (byte i = 0; i < _regions.Length; i++)
            _regions[i].Index = i;
    }
#endif
}