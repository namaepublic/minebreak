﻿using MightyAttributes;
#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;

public class InstanceManager : BaseManager
{
    // @formatter:off
    [Title("Instances")] 
    [SerializeField, FindObject, ReadOnly] private GameManager _gameManager;
    [SerializeField, FindObject, ReadOnly] private GUIManager _guiManager;
    [SerializeField, FindObject, ReadOnly] private TimeManager _timeManager;
    [SerializeField, FindObject, ReadOnly] private MoneyManager _moneyManager;
    [SerializeField, FindObject, ReadOnly] private PlayerLevelManager _playerLevelManager;
    [SerializeField, FindObject, ReadOnly] private UpgradesManager _upgradesManager;
    [SerializeField, FindObject, ReadOnly] private SellingManager _sellingManager;
    [SerializeField, FindObject, ReadOnly] private CameraManager _cameraManager;
    // @formatter:on

    private RegionManager m_regionManager;
    private LevelManager m_levelManager;
    
    private UnitsManager m_unitsManager;
    private DestructibleManager m_destructibleManager;

    private static InstanceManager m_instance;

    private static InstanceManager Instance
    {
        get
        {
#if UNITY_EDITOR
            if (!EditorApplication.isPlaying && m_instance == null)
                m_instance = ReferencesUtilities.FindFirstObject<InstanceManager>();
#endif
            return m_instance;
        }
    }
    
    public static GameManager GameManager => Instance._gameManager;
    public static GUIManager GUIManager => Instance._guiManager;
    public static TimeManager TimeManager => Instance._timeManager;
    public static MoneyManager MoneyManager => Instance._moneyManager;
    public static PlayerLevelManager PlayerLevelManager => Instance._playerLevelManager;
    public static UpgradesManager UpgradesManager => Instance._upgradesManager;
    public static CameraManager CameraManager => Instance._cameraManager;
    public static SellingManager SellingManager => Instance._sellingManager;
    
    public static RegionManager RegionManager
    {
        get
        {
#if UNITY_EDITOR
            if (!EditorApplication.isPlaying && Instance.m_regionManager == null)
                Instance.m_regionManager = ReferencesUtilities.FindFirstObject<RegionManager>();
#endif
            return Instance.m_regionManager;
        }
        set => Instance.m_regionManager = value;
    }

    public static LevelManager LevelManager
    {
        get
        {
#if UNITY_EDITOR
            if (!EditorApplication.isPlaying && Instance.m_levelManager == null)
                Instance.m_levelManager = ReferencesUtilities.FindFirstObject<LevelManager>();
#endif
            return Instance.m_levelManager;
        }
        set => Instance.m_levelManager = value;
    }

    public static UnitsManager UnitsManager
    {
        get
        {
#if UNITY_EDITOR
            if (!EditorApplication.isPlaying && Instance.m_levelManager == null)
                Instance.m_unitsManager = ReferencesUtilities.FindFirstObject<UnitsManager>();
#endif
            return Instance.m_unitsManager;
        }
        set => Instance.m_unitsManager = value;
    }
    
    public static DestructibleManager DestructibleManager
    {
        get
        {
#if UNITY_EDITOR
            if (!EditorApplication.isPlaying && Instance.m_levelManager == null)
                Instance.m_destructibleManager = ReferencesUtilities.FindFirstObject<DestructibleManager>();
#endif
            return Instance.m_destructibleManager;
        }
        set => Instance.m_destructibleManager = value;
    }    

    public override void Init() => m_instance = this;
}