﻿using MightyAttributes;
using UnityEngine;

public class CameraManager : BaseManager, IUpdateManager
{
    [SerializeField, FindObject, ReadOnly] private CameraShakeBehaviour _cameraShakeBehaviour;

    public override void Init() => _cameraShakeBehaviour.Init();

    public void UpdateManager() => _cameraShakeBehaviour.UpdateBehaviour();

    public void ShakeCamera() => _cameraShakeBehaviour.Shake();
}
