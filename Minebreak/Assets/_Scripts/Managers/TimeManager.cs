﻿using System;
using MightyAttributes;
using UnityEngine;

public class TimeManager : BaseManager, IFixedUpdateManager
{
    [Flags]
    private enum TimeState : byte
    {
        Nothing = 0,
        
        SlowMotion = 1 << 0,
        NextLevel = 1 << 1
    }

    public static readonly WaitForSeconds ReleaseTime = new WaitForSeconds(.3f);
    private static float m_fixedDeltaTime;

    [Title("Slow Motion")] [SerializeField]
    private AnimationCurve _slowMotionCurve;

    public float slowMotionDuration;

    [Title("Next Level")] public float nextLevelCooldown;

    [ShowNonSerialized(true)] private TimeState m_state;

    private float m_slowMotionTimer;

    private float m_nextLevelTimer;

    public override void Init()
    {
        m_fixedDeltaTime = Time.fixedDeltaTime;

        m_state = TimeState.Nothing;
        m_slowMotionTimer = 0;

        _slowMotionCurve.postWrapMode = WrapMode.ClampForever;
    }

    public void BeginSlowMotion()
    {
        m_state |= TimeState.SlowMotion;

        m_slowMotionTimer = 0;
        ChangeTimeScale(1);
    }

    public void EndSlowMotion()
    {
        m_state &= ~TimeState.SlowMotion;

        m_slowMotionTimer = 0;
        ChangeTimeScale(1);
    }    
    
    public void BeginNextLevelCooldown()
    {
        m_state |= TimeState.NextLevel;

        m_nextLevelTimer = 0;
    }

    public void EndNextLevelCooldown()
    {
        m_state &= ~TimeState.NextLevel;

        m_nextLevelTimer = 0;
        InstanceManager.GUIManager.BeginTransitionToNextLevel();
    }

    public static void ChangeTimeScale(float timeScale)
    {
        Time.timeScale = timeScale;
        Time.fixedDeltaTime = m_fixedDeltaTime * timeScale;
    }

    public void FixedUpdateManager()
    {
        if (m_state == TimeState.Nothing) return;

        var unscaledDeltaTime = Time.unscaledDeltaTime;
        
        if ((m_state & TimeState.SlowMotion) != 0)
        {
            ChangeTimeScale(_slowMotionCurve.Evaluate(m_slowMotionTimer / slowMotionDuration));

            m_slowMotionTimer += unscaledDeltaTime;

            if (m_slowMotionTimer >= slowMotionDuration) EndSlowMotion();
        }

        if ((m_state & TimeState.NextLevel) != 0)
        {
            m_nextLevelTimer += unscaledDeltaTime;
            
            if (m_nextLevelTimer >= nextLevelCooldown) EndNextLevelCooldown();
        }
    }
}