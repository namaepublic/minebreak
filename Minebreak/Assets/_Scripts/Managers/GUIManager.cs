﻿#if UNITY_EDITOR
using System.Linq;
#endif
using MightyAttributes;
using TMPro;
using UnityEngine;

public class GUIManager : BaseManager
{
    [SerializeField] private TextMeshProUGUI _moneyAmountText;
    [SerializeField, FindObject] private LevelBarBehaviour _levelBar;
    [SerializeField, FindObject] private RegionBarBehaviour _regionBar;
    [SerializeField, FindObjects] private UpgradeButtonBehaviour[] _upgradeButtons;

    // @formatter:off
    [Title("Animations")] 
    [SerializeField] private Animator _canvasAnimator;

    [SerializeField, ReadOnly, AnimatorParameter("Level")] private int _levelID;
    [SerializeField, ReadOnly, AnimatorParameter("In")] private int _inID;
    [SerializeField, ReadOnly, AnimatorParameter("Out")] private int _outID;
    // @formatter:on

    private UpgradesManager m_upgradesManager;

    [Button]
    public override void Init()
    {
        m_upgradesManager = InstanceManager.UpgradesManager;

        RefreshMoneyText();

        _levelBar.Init();
        _regionBar.Init();

        foreach (var upgradeButton in _upgradeButtons)
            upgradeButton.Init();
    }

    public void RefreshRegionDependantGUI()
    {
        _regionBar.Init();
        SetRegionProgression(InstanceManager.PlayerLevelManager.CurrentXP);

        foreach (var upgradeButton in _upgradeButtons)
        {
            upgradeButton.UpdateLevel();
            upgradeButton.SetEnable(m_upgradesManager.CanUpgrade(upgradeButton.Type) &&
                                    MoneyManager.CanSpend(m_upgradesManager.GetCost(upgradeButton.Type)));
        }
    }

    public void RefreshMoneyText() => _moneyAmountText.text = MoneyManager.DisplayAmount();

    public void SetLevelText(ushort level) => _levelBar.RefreshLevelText(level);

    public void SetLevelProgression(ulong currentXP) =>
        _levelBar.SetProgression(InstanceManager.PlayerLevelManager.GetLevelProgression(currentXP));

    public void SetRegionProgression(ulong impactCount) =>
        _regionBar.SetProgression(InstanceManager.RegionManager.GetRegionProgression(impactCount));

    public void RefreshUpgradeButtons()
    {
        foreach (var upgradeButton in _upgradeButtons)
            upgradeButton.SetEnable(m_upgradesManager.CanUpgrade(upgradeButton.Type) &&
                                    MoneyManager.CanSpend(m_upgradesManager.GetCost(upgradeButton.Type)));
    }

    public void RefreshUpgradeButton(UpgradeType type) =>
        _upgradeButtons[(byte) type].SetEnable(m_upgradesManager.CanUpgrade(type) &&
                                               MoneyManager.CanSpend(m_upgradesManager.GetCost(type)));

    public void BeginTransitionToNextLevel()
    {
        _canvasAnimator.SetBool(_levelID, true);
        _canvasAnimator.SetTrigger(_inID);
    }

    public void EndTransitionToNextLevel()
    {
        _canvasAnimator.SetBool(_levelID, true);
        _canvasAnimator.SetTrigger(_outID);
    }

    public void BeginTransitionToNextRegion()
    {
        if (!InstanceManager.GameManager.NextRegionAvailable()) return;
        
        _canvasAnimator.SetBool(_levelID, false);
        _canvasAnimator.SetTrigger(_inID);
    }

    public void EndTransitionToNextRegion()
    {
        _canvasAnimator.SetBool(_levelID, false);
        _canvasAnimator.SetTrigger(_outID);
    }

#if UNITY_EDITOR
    [OnInspectorGUI(false)]
    private void OnInspectorGUI()
    {
        if (_upgradeButtons == null) return;

        _upgradeButtons = _upgradeButtons.OrderBy(ub => (byte) ub.Type).ToArray();
    }
#endif
}