﻿using System.Linq;
using MightyAttributes;
using MightyAttributes.Utilities;
using UnityEngine;
using UnityEngine.SceneManagement;

public class RegionManager : BaseBrainManager
{
    [SerializeField] private RegionModel _region;

    [SerializeField, Reorderable(false, options: ArrayOption.DontFold | ArrayOption.DisableSizeField)]
    private LevelModel[] _levels;

    public BaseModel Region => _region;
    public byte RegionIndex => _region.Index;

    private void Start()
    {
        InstanceManager.RegionManager = this;
        Init();
    }

    public override void Init()
    {
        base.Init();
        InstanceManager.GUIManager.RefreshRegionDependantGUI();
        InstanceManager.GUIManager.EndTransitionToNextRegion();
    }
    
    public void BeginRegion() => LoadLevel(SavedDataServices.LevelIndex);

    public void LoadNextLevel()
    {
        var index = SavedDataServices.LevelIndex;

        if (IsLevelLoaded(index)) UnloadLevel(index);
        LoadLevel(++index);
    }

    public void LoadLevel(byte index, bool saveProgression = true)
    {
        InstanceManager.GameManager.Pause();

        if (index >= _levels.Length) index = 0;

        if (IsLevelLoaded(index)) UnloadLevel(index);
        if (saveProgression) SavedDataServices.LevelIndex = index;

        SceneManager.LoadScene(_levels[index].SceneIndex, LoadSceneMode.Additive);
    }

    public void UnloadLevel(byte index) => SceneManager.UnloadSceneAsync(_levels[index].SceneIndex);

    public bool IsLevelLoaded(byte index) => _levels[index].IsLoaded();

    public void RestartLevel() => LoadLevel(SavedDataServices.LevelIndex, false);

    public float GetRegionProgression(ulong impactCount)
    {
        var nextRegionCount = _region.ImpactsForNextRegion;

        return (float) impactCount / nextRegionCount;
    }

#if UNITY_EDITOR
    [OnInspectorGUI]
    private void PopulateLevels()
    {
        ArrayUtilities.CompareAndSetArray(ref _levels,
            ReferencesUtilities.FindAssetsOfType<LevelModel>().Where(l => l.Region == _region).ToArray());

        for (byte i = 0; i < _levels.Length; i++)
            _levels[i].Index = i;
    }
#endif
}