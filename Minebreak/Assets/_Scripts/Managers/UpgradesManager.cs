﻿using System;
using System.Numerics;
using MightyAttributes;
using MightyAttributes.Utilities;
#if UNITY_EDITOR
using System.Linq;
using MightyAttributes.Editor;
using UnityEditor;
#endif
using UnityEngine;

public class UpgradesManager : BaseManager
{
    [SerializeField, ReadOnly] private Upgrade[] _upgrades;

    public override void Init()
    {
    }

    public bool TryGetUpgrade(UpgradeType type, out Upgrade upgrade)
    {
        var index = (byte) type;
        if (_upgrades.Length > index)
        {
            upgrade = _upgrades[index];
            return true;
        }

        upgrade = null;
        return false;
    }

    public ushort GetLevel(UpgradeType type)
    {
        if (SavedDataServices.TryGetUpgradeLevel(type, InstanceManager.RegionManager.RegionIndex, out var level))
            return level;
        SavedDataServices.ResetUpgradeLevel(type, InstanceManager.RegionManager.RegionIndex);
        return 0;
    }

    public bool CanUpgrade(UpgradeType type) =>
        TryGetUpgrade(type, out var upgrade) && upgrade.CanUpgrade(InstanceManager.RegionManager.RegionIndex, GetLevel(type));

    public BigInteger GetCost(UpgradeType type) =>
        TryGetUpgrade(type, out var upgrade) ? upgrade.GetCost(InstanceManager.RegionManager.RegionIndex, GetLevel(type)) : BigInteger.Zero;

    public float GetValue(UpgradeType type) =>
        TryGetUpgrade(type, out var upgrade)
            ? upgrade.GetValue(InstanceManager.RegionManager.RegionIndex, GetLevel(type))
            : MathUtilities.ZERO;

    public bool TryToBuy(UpgradeType type)
    {
        if (!InstanceManager.MoneyManager.TryRemoveAmount(GetCost(type))) return false;

        SavedDataServices.IncreaseUpgradeLevel(type, InstanceManager.RegionManager.RegionIndex);
        InstanceManager.GUIManager.RefreshUpgradeButton(type);

        var value = GetValue(type);
        switch (type)
        {
            case UpgradeType.Bounciness:
                InstanceManager.UnitsManager.SetBounceForce(value);
                break;
            case UpgradeType.Damage:
                InstanceManager.UnitsManager.SetImpactDamage(value);
                break;
            case UpgradeType.Unit:
                InstanceManager.UnitsManager.AddUnit(GetValue(UpgradeType.Bounciness), GetValue(UpgradeType.Damage));
                break;
        }

        return true;
    }

#if UNITY_EDITOR
    [OnInspectorGUI]
    private void OnInspectorGUI()
    {
        if (!PopulateUpgrades()) return;

        if (InstanceManager.RegionManager == null || InstanceManager.RegionManager.Region == null) return;

        var names = Enum.GetNames(typeof(UpgradeType));
        var length = names.Length;

        MightyGUIUtilities.DrawLine(Color.grey);

        for (var i = 0; i < length; i++)
        {
            var type = (UpgradeType) i;
            if (!TryGetUpgrade(type, out var upgrade))
                continue;

            var level = GetLevel(type);
            EditorGUILayout.LabelField(upgrade.Name,
                $"level: {level} value: {upgrade.GetValue(InstanceManager.RegionManager.RegionIndex, level)}");
        }
    }

    private bool PopulateUpgrades()
    {
        _upgrades = ReferencesUtilities.FindAssetsOfType<Upgrade>();

        if (_upgrades.Length == 0) return false;

        _upgrades = _upgrades.OrderBy(x => (byte) x.Type).ToArray();
        return true;
    }
#endif
}