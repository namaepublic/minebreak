﻿using System.Numerics;
using MightyAttributes;
using UnityEngine;
using UnityEngine.Events;
#if UNITY_EDITOR
using UnityEditor;

#endif

public class MoneyManager : BaseManager
{
    [SerializeField] private UnityEvent _onAmountChange;

    public override void Init()
    {
    }

    public void AddAmount(BigInteger amount)
    {
        SavedDataServices.MoneyAmount = BigInteger.Add(SavedDataServices.MoneyAmount, amount);
        _onAmountChange.Invoke();
    }

    public bool TryRemoveAmount(BigInteger amount)
    {
        if (!CanSpend(amount)) return false;

        SavedDataServices.MoneyAmount = BigInteger.Subtract(SavedDataServices.MoneyAmount, amount);
        _onAmountChange.Invoke();
        return true;
    }

    public void ResetAmount()
    {
        SavedDataServices.MoneyAmount = BigInteger.Zero;
        _onAmountChange.Invoke();
    }

    public static bool CanSpend(BigInteger amount) => SavedDataServices.MoneyAmount >= amount;

    public static string DisplayAmount() => SavedDataServices.MoneyAmount.DisplayAsPowerOf10();

#if UNITY_EDITOR
    [OnInspectorGUI]
    private void OnInspectorGUI() => EditorGUILayout.LabelField("Money Amount", DisplayAmount());
#endif
}