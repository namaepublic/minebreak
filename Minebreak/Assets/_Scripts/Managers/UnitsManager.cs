﻿using System.Collections;
using MightyAttributes;
using UnityEngine;

public class UnitsManager : BaseManager
{
    [SerializeField] private bool _randomSpawn;

    [SerializeField] private Transform _unitsTransform;

    [SerializeField] private UnitBehaviour _unitPrefab;
    [SerializeField, ReadOnly] private UnitBehaviour[] _units;

    [SerializeField, FindObjectsWithTag("Spawn"), ReadOnly]
    private Transform[] _spawnPositions;

    private byte m_currentAmount;

    public override void Init()
    {
        InstanceManager.UnitsManager = this;
        
        ResetUnits();
    }

    public void ResetUnits()
    {
        m_currentAmount = 0;
        foreach (var unit in _units) unit.Init();
    }
    
    public void SpawnUnits() => StartCoroutine(SpawnUnitsCoroutine());

    private IEnumerator SpawnUnitsCoroutine()
    {
        var bounceForce = InstanceManager.UpgradesManager.GetValue(UpgradeType.Bounciness);
        var impactDamage = InstanceManager.UpgradesManager.GetValue(UpgradeType.Damage);
        for (var i = 0; i < (byte) InstanceManager.UpgradesManager.GetValue(UpgradeType.Unit); i++)
        {
            SpawnUnit(_units[i], bounceForce, impactDamage);
            yield return TimeManager.ReleaseTime;
        }
    }

    public void AddUnit(float bounceForce, float impactDamage)
    {
        if (m_currentAmount >= _units.Length) return;
        var unit = _units[m_currentAmount];
        unit.Init();
        SpawnUnit(unit, bounceForce, impactDamage);
    }

    private void SpawnUnit(UnitBehaviour unit, float bounceForce, float impactDamage)
    {
        if (_spawnPositions.Length == 0) return;
        
        var spawn = _randomSpawn ? _spawnPositions[Random.Range(0, _spawnPositions.Length)] : _spawnPositions[0];

        unit.Spawn(spawn.position);
        unit.SetBounceForce(bounceForce);
        unit.SetImpactDamage(impactDamage);
        m_currentAmount++;
    }

    public void SetBounceForce(float bounceForce)
    {
        for (var i = 0; i < m_currentAmount; i++)
            _units[i].SetBounceForce(bounceForce);
    }

    public void SetImpactDamage(float impactDamage)
    {
        for (var i = 0; i < m_currentAmount; i++)
            _units[i].SetImpactDamage(impactDamage);
    }

#if UNITY_EDITOR
    [Button]
    private void PopulateUnits()
    {
        if (!_unitsTransform) return;

        var regionManager = ReferencesUtilities.FindFirstObject<RegionManager>(gameObject.scene);
        if (!regionManager) return;
        var upgradesManager = InstanceManager.UpgradesManager;
        if (!upgradesManager) return;

        if (!upgradesManager.TryGetUpgrade(UpgradeType.Unit, out var upgrade)) return;

        while (_unitsTransform.childCount > 0)
            DestroyImmediate(_unitsTransform.GetChild(0).gameObject);

        var maxBotCount = (byte) upgrade.GetMaxValue(regionManager.RegionIndex);
        _units = new UnitBehaviour[maxBotCount];

        for (byte i = 0; i < maxBotCount; i++) _units[i] = Instantiate(_unitPrefab, _unitsTransform);
    }
#endif
}