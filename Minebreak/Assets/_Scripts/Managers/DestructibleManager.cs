﻿using System;
using System.Collections.Generic;
using System.Linq;
using Destructible2D;
using MightyAttributes;
using UnityEditor;
using UnityEngine;

[Serializable]
public class Destructible
{
    [Box] public ChunkBehaviour chunk;
    [Box] public float durability;
    
    [Box, ShowProperty] public float Damage { get; set; }
    [Box, ShowProperty] public bool Broken { get; set; }

    public int ChunkID { get; private set; }
    public Vector2 InitialPosition { get; private set; }

    private bool m_isInit;

    public void Init()
    {
        if (!m_isInit) ChunkID = chunk.gameObject.GetInstanceID();

        Damage = 0;
        Broken = false;

        chunk.Destructible = this;
        m_isInit = true;

        chunk.Init();

        InitialPosition = chunk.transform.position;
    }
}

public class DestructibleManager : BaseManager
{
    [SerializeField, FindObject, ReadOnly] private PoolBehaviour _particlesPool;

    [SerializeField] private Transform _chunksTransform;

    [SerializeField, Nest(NestOption.ContentOnly), ButtonArray]
    private Destructible[] _destructibles;

    public float ejectForce;

    private readonly Dictionary<int, Destructible> m_destructibleByID = new Dictionary<int, Destructible>();
    private readonly Dictionary<int, ChunkBehaviour> m_chunksByID = new Dictionary<int, ChunkBehaviour>();

    public Action<List<D2dDestructible>, D2dDestructible.SplitMode> OnSplitEnd { get; private set; }

    private Destructible m_currentDestructible;

    public override void Init()
    {
        OnSplitEnd = InternalOnSplitEnd;
        InstanceManager.DestructibleManager = this;

        m_chunksByID.Clear();

        foreach (var destructible in _destructibles)
        {
            destructible.Init();

            m_chunksByID[destructible.ChunkID] = destructible.chunk;
            m_destructibleByID[destructible.ChunkID] = destructible;
        }

        _particlesPool.Init();
    }

    public void OnImpact(int collidedID, float damage)
    {
        if (!m_chunksByID.TryGetValue(collidedID, out var chunk))
            return;

        chunk.IncreaseDamage(damage);

        if (!m_destructibleByID.TryGetValue(collidedID, out m_currentDestructible) &&
            !m_destructibleByID.TryGetValue(chunk.Destructible.ChunkID, out m_currentDestructible)
            || m_currentDestructible.Broken) return;

        InstanceManager.CameraManager.ShakeCamera();
        InstanceManager.SellingManager.Sell(damage);
        InstanceManager.PlayerLevelManager.IncreaseImpactCount();

        _particlesPool.ActivateNext();

        m_currentDestructible.Damage += damage;
    }

    private void InternalOnSplitEnd(List<D2dDestructible> destructibles, D2dDestructible.SplitMode splitMode)
    {
        foreach (var destructible in destructibles)
        {
            destructible.transform.SetParent(_chunksTransform);

            var chunk = destructible.GetComponent<ChunkBehaviour>();
            m_chunksByID[destructible.gameObject.GetInstanceID()] = chunk;

            chunk.Init();

            if (m_currentDestructible.Broken)
                chunk.Eject(ejectForce);

            chunk.Destructible = m_currentDestructible;
        }

        if (m_currentDestructible.Broken || m_currentDestructible.durability >= m_currentDestructible.Damage) return;

        Break();
    }

    private void Break()
    {
        m_currentDestructible.Broken = true;

        EjectChunks();
        
        InstanceManager.SellingManager.Sell(m_currentDestructible.durability);
        _particlesPool.ActivateNext();

        if (_destructibles.Any(x => !x.Broken)) return;

        InstanceManager.TimeManager.BeginSlowMotion();
        InstanceManager.TimeManager.BeginNextLevelCooldown();
    }

    private void EjectChunks()
    {
        foreach (var chunkData in m_chunksByID)
            chunkData.Value.EjectFromInitialChunk(ejectForce);
    }

#if UNITY_EDITOR
    [OnInspectorGUI]
    private void OnInspectorGUI()
    {
        if (m_chunksByID == null) return;

        foreach (var chunk in m_chunksByID)
        {
            GUI.enabled = false;
            EditorGUILayout.ObjectField(chunk.Value, typeof(ChunkBehaviour), true);
            GUI.enabled = true;
        }
    }
#endif
}