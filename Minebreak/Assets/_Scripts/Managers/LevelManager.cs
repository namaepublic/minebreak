﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelManager : BaseBrainManager
{
    [SerializeField] private LevelModel _level;
    
    public LevelModel Level => _level;
    public byte LevelIndex => _level.Index;
    
    private void Start()
    {
        InstanceManager.LevelManager = this;
        Init();
    }

    public override void Init()
    {
        base.Init();
        InstanceManager.UnitsManager.ResetUnits();
        InstanceManager.GUIManager.EndTransitionToNextLevel();
    }

    public void BeginLevel()
    {
        InstanceManager.UnitsManager.SpawnUnits();
        InstanceManager.GameManager.Play();
    }

    public void UnloadLevel() => SceneManager.UnloadSceneAsync(_level.SceneIndex);
}