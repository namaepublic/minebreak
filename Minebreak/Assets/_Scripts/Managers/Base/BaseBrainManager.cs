﻿using MightyAttributes;
using UnityEngine;

public abstract class BaseBrainManager : BaseManager, IUpdateManager, IFixedUpdateManager
{
    [SerializeField, FindObjects(ignoreSelf: true), Reorderable(false, options: ArrayOption.DisableSizeField)]
    private BaseManager[] _managers;

    public override void Init()
    {
        foreach (var manager in _managers)
            manager.Init();
    }

    public void UpdateManager()
    {
        foreach (var manager in _managers)
            if (manager is IUpdateManager updateManager)
                updateManager.UpdateManager();
    }

    public void FixedUpdateManager()
    {
        foreach (var manager in _managers)
            if (manager is IFixedUpdateManager fixedUpdateManager)
                fixedUpdateManager.FixedUpdateManager();
    }
}