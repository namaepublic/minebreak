﻿public interface IManager
{
}

public interface IUpdateManager : IManager
{
    void UpdateManager();
}

public interface IFixedUpdateManager : IManager
{
    void FixedUpdateManager();
}
