﻿using System.Numerics;

public static class BigIntegerUtilities 
{
    public static string DisplayAsPowerOf10(this BigInteger bigInteger, int maxDigit = 3)
    {
        var amount = bigInteger.ToString();

        var length = amount.Length - 1;
        if (length > maxDigit) 
            amount = $"{amount.Substring(0, 1)}.{amount.Substring(1, 2)}e{length}";

        return amount;
    }
}
