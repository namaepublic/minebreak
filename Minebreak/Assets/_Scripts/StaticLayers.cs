﻿public static class StaticLayers
{
    public const int UNIT_LAYER = 8;
    public const int WALL_LAYER = 9;
    public const int DESTRUCTIBLE_LAYER = 10;
}