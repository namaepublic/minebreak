﻿using UnityEngine;

[CreateAssetMenu(menuName = "Model/Region Model", fileName = "RegionModel")]
public class RegionModel : BaseModel
{
    [SerializeField] private ulong _impactsForNextRegion;
    
    public ulong ImpactsForNextRegion => _impactsForNextRegion;
}