﻿using MightyAttributes;
using UnityEngine;
using UnityEngine.SceneManagement;

public abstract class BaseModel : ScriptableObject
{
    [SerializeField, ReadOnly] private byte _index;
    [SerializeField, SceneDropdown] private byte _scene;

    public byte Index
    {
        get => _index;
#if UNITY_EDITOR
        set => _index = value;
#endif
    }
    
    public byte SceneIndex => _scene;
    
    public bool IsLoaded() => SceneManager.GetSceneByBuildIndex(SceneIndex).isLoaded;
}
