﻿using UnityEngine;

[CreateAssetMenu(menuName = "Model/Level Model", fileName = "LevelModel")]
public class LevelModel : BaseModel
{
    [SerializeField] private RegionModel _region;

    public RegionModel Region => _region;
}