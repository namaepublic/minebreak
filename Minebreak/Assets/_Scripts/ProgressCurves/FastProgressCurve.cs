﻿using System;
using UnityEngine;

[CreateAssetMenu(fileName = "FastProgressCurve", menuName = "Progress Curve/Fast Progress Curve")]
public class FastProgressCurve : BaseProgressCurve
{
    public override double Evaluate(double x)
    {
        if (x < _offset.x) return 0;
        return Math.Pow((x - _offset.x) * _multiplier, _curveForce) + _offset.y;
    }

    public override double Revert(double x)
    {
        if (x < _offset.y) return 0;
        return Math.Pow((x - _offset.y), _curveForce) / _multiplier + _offset.x;
    }
    
#if UNITY_EDITOR
    protected override string GetFormula() => $"((x - {_offset.x}) * {_multiplier})^{_curveForce} + {_offset.y}";
#endif
}